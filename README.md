What I have deployed:
- Two directories ansible and sandbox
- Installed ansible locally and added localhost to the host list. The file at ./ansible/prerequisites/ansible.hosts is the same as my local /etc/ansible/hosts
- Wrote an ansible-playbook called ./ansible/deploy_server.yaml that puts the nginx configuration, self-signed certificate and docker-compose.yml to localhost to the appropriate place
- Whenever the nginx config file is changed or docker-compose.yml file is updated, it'll trigger a restart of docker-compose so the services that host the actual web server running gets restarted with appropriate changes being made

Contents of directory "sandbox"
- I wrote a simple go program that starts an http server at port 6000 that just prints "BYTECUBED DEVOPS CHALLENGE" and then packaged it to be a docker image
- The contents in ./sandbox/nginx/ is automatically managed by ansible playbook above. The docker container that runs nginx just uses these configs for its own deployment
- ./sandbox/docker-compose.yml is the main file. It contains two docker containers(our website docker image and nginx image) that are linked together
- The actual nginx config located at ./sandbox/nginx/webserver.conf is the one that handles all the routing. Anytime the user tries to access the website at a particular URL(in our case, it's http://localhost:8000), it just redirects to https). If, instead the user directly goes to https://localhost:9000, it works as expected


Pipeline:
- Run ansible playbook using "ansible-playbook deploy_server.yml"
- This will start docker containers and start running our website via a docker container

Assumptions:
- localhost already has docker installed and ansible installed
- I ran this on a centos VM in Virtualbox so I had to configure additional settings via virtualbox settings so i could route from my internet browser on my windows box(host) to the docker VM(that's hosted in centos VM that is in turn hosted in virtualbox)
- Virtualbox > Centos 7 VM(right click) > Settings > Network > Port Forwarding and after this, i had to forward port 8000:8000 and 443:9000 respectively

Commands executed in order:
- yum install ansible
- cd ./sandbox/webserver; docker build -t webserver .
- cd ./ansible; ansible-playbook deploy_server.yaml 
- Go to your internet browser like chrome on your host box(for me, it was my windows destkop) and then go to "localhost:8000". What this essentially does is routes the traffic from my windows box to my centos VM to port 8000(because of the mapping i did in my virtualbox settings) and then nginx routes this port to 443(HTTPS)
- You can also go to localhost:9000 directly and this will also get routed to port 443(HTTPS) as well
- To test if port 443 is listening, just run ./test/check_port.sh
