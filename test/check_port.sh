#!/bin/bash

netstat -tuplen | grep 443 >/dev/null 2>/dev/null
if [ "$(echo $?)" -eq "0" ]
then
    echo "Port 443 is listening"
else
    echo "Port 443 is not listening"
fi
